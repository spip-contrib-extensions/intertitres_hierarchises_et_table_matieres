<?php

/**
 *   +----------------------------------+
 *    Nom :   Table des matieres
 *   +----------------------------------+
 *    Date : aout 2004
 *    Auteur :  Mortimer Porte mortimer.pa@free.fr
 *    Modifications: Bertrand Marne bmarne@gmail.com
 *    Modifications: Stéphane Deschamps spip@nota-bene.org
 *    Modifications: Christian Paulus (CP) paladin at quesaco.org
 *      pour les ancres nommées.
 *   +-------------------------------------+
 *    Fonctions de ce filtre :
 *     affiche une table des matières ou génère automatiquement la numérotation des titres.
 *   +-------------------------------------+
 *
 * Pour toute suggestion, remarque, proposition d'ajout
 * reportez-vous au forum de l'article :
 * https://contrib.spip.net/Generation-automatique-de
 *
 * Petites modifications de Bertrand Marne (mise en plugin)
 * - prise en compte des dièses:
 * les * donnent des titres non numérotés, les # des titres numérotés
 * Mais attention ils sont tous décomptés pareil pour l'arborescence de la table
 * des matières et la numérotation, qu'ils soient numérotés (#) ou non (*).
 *
 * - prise ne compte des titres comme <hx class="spip"> où x>3
 *
 * - encore une modif, très moche: pour permettre de n'afficher que la table,
 * quand on utilise ceci comme un filtre avec unn second paramètre:
 * [(#TEXTE|intertitrestdm_table_des_matieres{table_seule})]
 * - ajout d'une fonction qui converti les intertitres des enluminures en
 * intertitres compatibles avec cette contrib'
 *
 * Petites modifications de Stéphane Deschamps
 * - prise en compte des niveaux de titres si ils sont déclarés dans mes_fonctions ou mes_options par $GLOBALS['debut_intertitre']
 *
 * CP: pour les ancres nommées :
 * {@link intertitrestdm_composer_ancre()}
 * Les ancres sont calculées à partir du contenu du titre.
 * Si deux titres ont le même contenu, un chiffre est ajouté
 * à l'ancre du second (et suivant).
 * Ce chiffre est la position du titre dans la page.
 * Les anciennes ancres 'aNNN' sont conservées pour compatibilité
 * avec les modèles accompagnant le plugin (extrait, ...)
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_ANCHOR_LEN_MAX')) {
	define('_ANCHOR_LEN_MAX', 35);
}

/**
 * @deprecated
 */
function extrait_titres($texte) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_titres($texte ?? '');
}
/**
 * @deprecated
 */
function bien_guillemeter($extrait, $languepays = 'fr') {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_bien_guillemeter($extrait ?? '', $languepays);
}
/**
 * @deprecated
 */
function extrait_emphaseforte($texte, $type_guillemets = 'fr') {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_emphaseforte($texte ?? '', $type_guillemets);
}
/**
 * @deprecated
 */
function extrait_emphase($texte, $type_guillemets = 'fr') {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_emphase($texte ?? '', $type_guillemets);
}
/**
 * @deprecated
 */
function extrait_un_titre($texte, $ancre = '   #  ? ! ') {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_un_titre($texte ?? '', $ancre);
}

/**
 * @deprecated
 */
function extrait_liens($texte) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_liens($texte ?? '');
}
/**
 * @deprecated
 */
function extrait_de_texte($texte, $debut = 0, $taille = 20) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_de_texte($texte ?? '', intval($debut), intval($taille));
}

/**
 * @deprecated
 */
function extrait_partie($texte, $ancre = '   #  ? ! ', $debut = 0, $taille = '') {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_extrait_partie($texte ?? '', $ancre, intval($debut), $taille ? $taille : null);
}
/**
 * @deprecated
 */
function nettoie_des_modeles($texte) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_nettoie_des_modeles($texte ?? '');
}
/**
 * @deprecated
 */
function table_des_matieres($texte, $tdm, $url) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_table_des_matieres($texte ?? '', $tdm ? true : false, $url ?? '');
}
/**
 * @deprecated
 */
function intertitre_tdm_preserver_tags_typo($str) {
	include_spip('intertitrestdm_fonctions');
	return intertitrestdm_preserver_tags_typo($str ?? '');
}


