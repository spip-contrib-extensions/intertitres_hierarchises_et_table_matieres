<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/intertitres_hierarchises_et_table_matieres.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aller_directement_a_' => 'Aller directement à « @titre@ »',

	// I
	'inserer_extrait' => 'un extrait du texte d’un article',
	'inserer_partie' => 'un extrait d’une section d’un article',
	'inserer_renvoi' => 'un renvoi vers une section d’un article',
	'inserer_table' => 'la table des matières d’un article',

	// L
	'label_article' => 'Article cible',
	'label_debut' => 'Premier mot affiché (décompte à partir de zéro)',
	'label_partie' => 'Partie de l’article sous la forme X.Y.Z numériques',
	'label_taille' => 'Nombre de mots affichés (décompte à partir de zéro)',

	// T
	'title_lien_table_matieres' => 'Aller à la table des matières de l’article',
	'titre_table_matieres' => 'Table des matières'
);
