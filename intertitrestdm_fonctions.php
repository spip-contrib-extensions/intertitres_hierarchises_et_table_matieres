<?php

/*
 *   +----------------------------------+
 *    Nom du Filtre :    extrait_titres,extrait_emphaseforte...
 *   +----------------------------------+
 *    Date : 19 décembre 2006
 *    Auteur :  Bertrand Marne (extraction à sciencesnat point org)
 *   +-------------------------------------+
 *   Fonctions de ces filtres :
 *   Ces filtres extraient des infos des articles comme:
 *   Les titres de parties, les mots en emphase ou les URL
 *   Il sert à faire ressortir les éléments sémantiques (taggés
 *   par les raccourcis Spip, donc s'utilise avec #TEXTE*)
 *   +-------------------------------------+
 *
 * Pour toute suggestion, remarque, proposition d'ajout
 * reportez-vous au forum de l'article :
*/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param string $texte
 * @return string
 */
function intertitrestdm_extrait_titres(string $texte): string {
	$sortie = '';
	if (preg_match_all('/\{\{\{(.*?)\}\}\}/', $texte, $matches)) {
		foreach ($matches[1] as $key => $val) {
			$sortie .= '-' . $val . "\n";
		}
	}

	return $sortie;
}

/**
 * règles : https://fr.wikipedia.org/wiki/Guillemet#Autres_langues_et_autres_pays
 * entités : https://www.w3.org/wiki/Common_HTML_entities_used_for_typography
 * @param string $extrait
 * @param string $languepays
 * @return string
 */
function intertitrestdm_bien_guillemeter(string $extrait, string $languepays = 'fr'): string {
	switch ($languepays) {
	case 'fr': // français
	case 'pt': // Portugal
		$niveau1_ouvrant = '&laquo;&nbsp;';
		$niveau1_fermant = '&nbsp;&raquo;';
		$niveau2_ouvrant = '&ldquo;';
		$niveau2_fermant = '&rdquo;';
		$niveau3_ouvrant = '&rsquo;';
		$niveau3_fermant = '&lsquo;';
		break;
	case 'en': // anglais
	case 'eo': // esperanto
	case 'nl': // néerlandais
	case 'br': // Brésil
		$niveau1_ouvrant = '&ldquo;';
		$niveau1_fermant = '&rdquo;';
		$niveau2_ouvrant = '&lsquo;';
		$niveau2_fermant = '&rsquo;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	case 'es': // Espagne
		$niveau1_ouvrant = '&laquo;';
		$niveau1_fermant = '&raquo;';
		$niveau2_ouvrant = '&ldquo;';
		$niveau2_fermant = '&rdquo;';
		$niveau3_ouvrant = '&lsquo;';
		$niveau3_fermant = '&rsquo;';
		break;
	case 'it': // italien
		$niveau1_ouvrant = '&laquo;&nbsp;';
		$niveau1_fermant = '&nbsp;&raquo;';
		$niveau2_ouvrant = '&ldquo;';
		$niveau2_fermant = '&rdquo;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	case 'da': // dannois
	case 'de': // allemand
	case 'hr': // croate
	case 'sl': // slovène
		$niveau1_ouvrant = '&raquo;';
		$niveau1_fermant = '&laquo;';
		$niveau2_ouvrant = '&gt;';
		$niveau2_fermant = '&lt;';
		$niveau3_ouvrant = '&sbquo;';
		$niveau3_fermant = '&lsquo;';
		break;
	case 'bg': // bulgare
	case 'cs': // tchèque
		$niveau1_ouvrant = '&bdquo;';
		$niveau1_fermant = '&ldquo;';
		$niveau2_ouvrant = '&gt;';
		$niveau2_fermant = '&lt;';
		$niveau3_ouvrant = '&sbquo;';
		$niveau3_fermant = '&lsquo;';
		break;
	case 'hu': // hongrois
	case 'pl': // polonais
		$niveau1_ouvrant = '&bdquo;';
		$niveau1_fermant = '&rdquo;';
		$niveau2_ouvrant = '&gt;';
		$niveau2_fermant = '&lt;';
		$niveau3_ouvrant = '&sbquo;';
		$niveau3_fermant = '&lsquo;';
		break;
	case 'no': // norvégien
		$niveau1_ouvrant = '&laquo;&nbsp;';
		$niveau1_fermant = '&nbsp;&raquo;';
		$niveau2_ouvrant = '&lt;';
		$niveau2_fermant = '&gt;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	case 'fi': // finnois
	case 'sv': // suédois
		$niveau1_ouvrant = '&ldquo;';
		$niveau1_fermant = '&ldquo;';
		$niveau2_ouvrant = '&lsquo;';
		$niveau2_fermant = '&lsquo;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	case 'ch': // Suisse
	case 'li': // Liechtenstein
		$niveau1_ouvrant = '&laquo;';
		$niveau1_fermant = '&raquo;';
		$niveau2_ouvrant = '&ldquo;';
		$niveau2_fermant = '&rdquo;';
		$niveau3_ouvrant = '&rsquo;';
		$niveau3_fermant = '&lsquo;';
		break;
	case 'be': // biélorusse
	case 'ru': // russe
	case 'uk': // ukrainien
		$niveau1_ouvrant = '&laquo;';
		$niveau1_fermant = '&raquo;';
		$niveau2_ouvrant = '&bdquo;';
		$niveau2_fermant = '&ldquo;';
		$niveau3_ouvrant = '&sbquo;';
		$niveau3_fermant = '&rsquo;';
		break;
	case 'jp': // japonais
		$niveau1_ouvrant = '&#x300c;';
		$niveau1_fermant = '&#x300d;';
		$niveau2_ouvrant = '&#xff62;';
		$niveau2_fermant = '&#xff63;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	case 'zh': // mandarin
		$niveau1_ouvrant = '&#x300a;';
		$niveau1_fermant = '&#x300b;';
		$niveau2_ouvrant = '&#x2329;';
		$niveau2_fermant = '&#x232a;';
		$niveau3_ouvrant = '&Prime;';
		$niveau3_fermant = '&Prime;';
		break;
	default: // pandunia ; informatique
		$niveau1_ouvrant = '&Prime;';
		$niveau1_fermant = '&Prime;';
		$niveau2_ouvrant = '&prime;';
		$niveau2_fermant = '&prime;';
		$niveau3_ouvrant = '`';
		$niveau3_fermant = '`';
		break;
	}
	// décalage des niveaux 2 d'abord
	$extrait = str_replace(
		[$niveau2_ouvrant, $niveau2_fermant],
		[$niveau3_ouvrant, $niveau3_fermant],
		htmlentities($extrait, ENT_NOQUOTES | ENT_SUBSTITUTE)
	) ;
	// décalage des niveaux 1 ensuite
	$extrait = str_replace(
		[$niveau1_ouvrant, $niveau1_fermant],
		[$niveau2_ouvrant, $niveau2_fermant],
		$extrait
	) ;
	// encadrement de l'ensemble enfin
	return $niveau1_ouvrant . $extrait . $niveau1_fermant;
}

/**
 * @param string $texte
 * @param string $type_guillemets
 * @return string
 */
function intertitrestdm_extrait_emphaseforte(string $texte, string $type_guillemets = 'fr'): string {
	// protection des titres
	$texte = preg_replace('/(\{\{\{)(.*?)(\}\}\})/', '', $texte);
	// c'est parti
	$sortie = '';
	if (preg_match_all('/\{\{(.*?)\}\}/', $texte, $matches)) {
		foreach ($matches[1] as $key => $val) {
			$sortie .= intertitrestdm_bien_guillemeter($val, $type_guillemets) . ' ; ';
		}
	}
	return $sortie;
}

/**
 * @param string $texte
 * @param string $type_guillemets
 * @return string
 */
function intertitrestdm_extrait_emphase(string $texte, string $type_guillemets = 'fr'): string {
	// protection des titres et emphases fortes
	$texte = preg_replace('/(\{\{)(.*?)(\}\})/', '', $texte);
	// c'est parti
	$sortie = '';
	if (preg_match_all('/\{(.*?)\}/', $texte, $matches)) {
		foreach ($matches[1] as $key => $val) {
			$sortie .= intertitrestdm_bien_guillemeter($val, $type_guillemets) . ' ; ';
		}
	}
	return $sortie;
}

/**
 * @param string $texte
 * @return string
 */
function intertitrestdm_extrait_liens(string $texte): string {
	// protection des codes
	$texte = preg_replace('/(<code>)(.*?)(<\/code>)/', '', $texte);
	// protection des ancres
	$texte = preg_replace('/(\[.*?\<-])/', '', $texte);
	// protection des notes
	$texte = preg_replace('/(\[\[)(.*?)(\]\])/', '', $texte);
	// c'est parti
	$sortie = '';
	if (preg_match_all('/(\[.*?\])/', $texte, $matches)) {
		foreach ($matches[1] as $key => $val) {
			$sortie .= $val . "\n\n";
		}
	}
	return $sortie;
}

/**
 * pour le 3ieme intertitre produit par ce balisage SPIP : {{{** foo bar }}}
 * le plugin seul produit la ligne HTML suivante :
 *  <h4 class="spip"><a id="foo-bar-2" name="foo-bar-2"></a><a id="a2.1" name="a2.1"> foo bar </h4>
 * tandis qu'en presence du plugin Sommaire Automatique on a ce HTML (en une ligne et non deux) :
 *  <h4 class="spip" id="foo-bar"><a id="foo-bar-2" name="foo-bar-2"></a><a id="a2.1" name="a2.1"> foo bar
 *  <a class="sommaire-back sommaire-back-6" href="#s-foo-bar" title="Retour au sommaire"></a></h4>
 *
 * @param string $texte
 * @param string $ancre
 * @return string
 */
function intertitrestdm_extrait_un_titre(string $texte, string $ancre = '   #  ? ! '): string {
	#spip_log('titre goo', 'itdm');
	if (
		preg_match(
			"#<h(\d) class=\"spip\".+<a id=\"a$ancre\" name=\"a$ancre\"></a>(.*?)</h\\1>#",
			$texte,
			$matches
		)
	) {
		#spip_log('titre id: a' . $ancre, 'itdm');
		return textebrut($matches[2]);
	}

	if (
		preg_match(
			"#<h(\d) class=\"spip\".+id=\".*$ancre.*\".*>(.*?)</h\\1>#",
			$texte,
			$matches
		)
	) {
		#spip_log('titre id: ' . $ancre, 'itdm');
		return textebrut($matches[2]);
	}
	return '';
}

/**
 * @param string $texte
 * @param int $debut
 * @param int $taille
 * @return string
 */
function intertitrestdm_extrait_de_texte(string $texte, int $debut = 0, int $taille = 20): string {
	$mots = explode(' ', textebrut($texte));
	$extrait = implode(' ', array_slice($mots, $debut, $taille));
	return $extrait;
}

/**
 * pour le 3ieme intertitre produit par ce balisage SPIP : {{{## foo bar }}}
 * le plugin seul produit la ligne HTML suivante :
 *  <h4 class="spip"><a id="foo-bar-2" name="foo-bar-2"></a><a id="a2.1" name="a2.1"> 1.2- foo bar </h4>
 * tandis qu'en presence du plugin Sommaire Automatique on a ce HTML (en une ligne et non deux) :
 *  <h4 class="spip" id="t1-2-foo-bar"><a id="foo-bar-2" name="foo-bar-2"></a><a id="a2.1" name="a2.1"> 1.2- foo bar
 *  <a class="sommaire-back sommaire-back-6" href="#s-t1-2-foo-bar" title="Retour au sommaire"></a></h4>
 * @param string $texte
 * @param string $ancre
 * @param int $debut
 * @param int|null $taille
 * @return string
 */
function intertitrestdm_extrait_partie(string $texte, string $ancre = '   #  ? ! ', int $debut = 0, ?int $taille = null): string {
	spip_log('partie goo', 'itdm');
	$partie = '';
	if (
		preg_match('#<h(\d) class="spip".+<a id="a' . $ancre . '" name="a' . $ancre .
		'"></a>.*</h\\1>(.*?)<h\\1 class="spip".*>#s', $texte, $matches)
	) {
		#spip_log('partie id: a' . $ancre, 'itdm');
		$partie = $matches[2];
	} elseif (
		preg_match('#<h(\d) class="spip".+id=".*' . $ancre .
			'.*".+</h\\1>(.*?)<h\\1 class="spip".*>#s', $texte, $matches)
	) {
		#spip_log('partie id: ' . $ancre, 'itdm');
		$partie = $matches[2];
	}

	if ($partie) {
		if (!$taille) {
			$taille = str_word_count($partie);
		}
		$extrait = intertitrestdm_extrait_de_texte($partie, $debut, $taille);
	}
	else {
		$extrait = '';
	}
	return $extrait;
}

/**
 * @param string $texte
 * @return string
 */
function intertitrestdm_nettoie_des_modeles(string $texte): string {
	// retire les modeles du plugin pour éviter les plantages circulaires
	$texte = preg_replace('/<(extrait|extrait_partie|renvoi|table_des_matieres)(.*?)>/', '', $texte);
	// retire les notes du texte, pour éviter les doublons de notes !
	$texte = preg_replace('/(\[\[)(.*?)(\]\])/', '', $texte);
	return $texte;
}


/**
 * @param string $texte
 * @param bool $tableseule
 * @param string $url_article
 * @return string
 */
function intertitrestdm_table_des_matieres(?string $texte, bool $tableseule = false, string $url_article = ''): string {
	static $pass = 0;
	$pass++;

	$texte = $texte ?? '';

	// $GLOBALS['debut_intertitre'] et $GLOBALS['fin_intertitre']
	// n'existent plus par defaut en SPIP 3+, mais leur definition permet de personaliser le fonctionnement du site
	if (!isset($GLOBALS['debut_intertitre'])) {
		$GLOBALS['debut_intertitre'] = '<h3 class="spip">';
		$GLOBALS['fin_intertitre'] = '</h3>';
	}

	// définition de la balise pour les titres des sections %num% sera remplacé
	// par la profondeur de la section
	// les raccourcis soient remplacés par des headlines (<hx>)
	$css_debut_intertitre = "\n<h%num% class=\"spip\">";
	$css_fin_intertitre = "</h%num%>\n";

	// on trouve combien ajouter au level pour être dans le bon niveau de titres quand on génère la balise <hx>
	// sinon par defaut on ajoute 2 pour garder les niveaux du script original
	if ($GLOBALS['debut_intertitre']) {
		$find = preg_match('/(\<h)([0-9])/', $GLOBALS['debut_intertitre'], $matches);
		if ($matches) {
			$level_base = $matches[2] - 1; // on déduit 1 pour être au bon niveau ensuite : ce sera 1 + nombre d'astérisques trouvées
		}
	}
	if (!isset($level_base)) {
		$level_base = 2;
	}

	// on cherche les noms de section commençant par des * et #
	// astuce des trim trouvée là : https://contrib.spip.net/Generation-automatique-de#forum383092
	$my_debut_intertitre = trim($GLOBALS['debut_intertitre']);
	$my_fin_intertitre = trim($GLOBALS['fin_intertitre']);

	// pour que les différents niveaux d'intertitres soient gérés quand on repasse sur le texte dans le cadre d'un filtre avec tableseule
	if ($tableseule) {
		$my_debut_intertitre = trim("\n<h([3-9]) class=\"spip\">");
		$my_fin_intertitre = trim("</h[3-9]>\n");
	}

	// on cherche les noms de section commençant par des * et #
	$count = preg_match_all(
		// préserver les liens éventuels présents dans le titre
		// À vérifier le $ref qui semblait s'en servir ?
		//"(($my_debut_intertitre([\\*#]*)(.*?)(<(.*?)>)?$my_fin_intertitre))",
		"(($my_debut_intertitre([\\*#]*)(.*?)$my_fin_intertitre))",
		$texte,
		$matches
	);

	//initialisation du compteur
	$cnt[0] = 0;

	// initialisation du code de la table des matières
	// s'articule autour d'un <a id=""> et d'un <ul>
	$table = "\n<a id=\"table_des_matieres\" name=\"table_des_matieres\"></a><div id=\"tablematiere\">\n<ul>";
	$lastlevel = 1;
	$cite[''] = '';

	$ajout = 0;
	// décalle le matching quand on repasse sur le texte avec tableseule
	if ($tableseule) {
		$ajout = 1;
	}

	// pour chaque titre trouvé
	for ($j = 0; $j < $count; $j++) {
		$ancre = intertitrestdm_composer_ancre($matches[0][$j], $pass, $j);
		$level = $matches[2 + $ajout][$j];

		// quand tableseule, le niveau est "recréé" à partir du nombre du headline (ex <h4> donne niveau 2)
		if ($tableseule) {
			$level = str_repeat('*', $matches[2][$j] - 2);
		}

		// pour tenir compte des {{{ }}} sans * ou # et donc qu'un name
		// leur soit ajouté, et qu'ils soient quand même dans la table des matières
		if (strlen($level) == 0) {
			$level = '*';
		}
		$titre = $matches[3 + $ajout][$j];
		if (!defined('_INTERTITRES_TDM_PRESERVER_TAGS_TYPO')) {
			// traitement standard: on supprime tous les tags sur les titres de la table des matieres
			$titre_lien = strip_tags($matches[0][$j]);
		} else {
			// traitement expérimental: preserver certains tags dans  la table des matieres
			$titre_lien = intertitrestdm_preserver_tags_typo($matches[0][$j]);
		}

		// Si tableseule alors on vire les <a id=''></a> des titres
		if ($tableseule) {
			$titre = preg_replace("#(<a id=')(.*?)('></a>)#", '', $titre);
		}
		if (isset($matches[5 + $ajout])) {
			$ref = $matches[5 + $ajout][$j];
		}

		// si tableseule alors le $ref correspond au contenu du <a id=''></a>... Je sais pas si ça marche: pas testé ! :o)
		if ($tableseule) {
			$ref = '';
			if (
				preg_match(
					"#(<a id=')(.*?)(' id=')(.*?)('></a>)#",
					$matches[3 + $ajout][$j],
					$tsmatches
				)
			) {
				$ref = $tsmatches[2];
			}
		}
		#spip_log('ref: '.$ref, 'itdm');

		if (strlen($level) == 1) {
			// on est au niveau de base
			// on réinitialise les compteurs
			$cnt = array_slice($cnt, 0, 1);

			// on génère le titre et le numéros
			$numeros = ++$cnt[0];

			// on teste si le level contient des # pour savoir si l'on affiche les
			// numéros avec le titre ou non (#->numéros affichés)
			if (preg_match('/#+/', $level)) {
				$titre = $numeros . '- ' . $titre;
			}
		} else {
			// on est à un niveau plus profond
			// on construit le numéro
			$numeros = $cnt[0] . '.';
			for ($i = 1; $i < strlen($level) - 1; $i++) {
				$numeros .= $cnt[$i] . '.';
			}
			$cnt[$i] = $cnt[$i] ?? 0;
			$numeros = $numeros . (++$cnt[$i]);
			// On réinitialise les niveaux inférieurs:
			$cnt = array_slice($cnt, 0, $i + 1);

			// on génère le titre
			// on teste si le level contient des # pour savoir si l'on affiche les
			// numéros avec le titre ou non (#->numéros affichés)
			if (preg_match('/#+/', $level)) {
				$titre = $numeros . '- ' . $titre;
			}
		}

		// gestion de la liste dans la table
		if ($lastlevel < strlen($level)) {
			// on ouvre une sous liste
			$table .= "<ul>\n";
		}

		if ($lastlevel > strlen($level)) {
			// on doit fermer les derniers niveaux
			for ($i = 0; $i < ($lastlevel - strlen($level)); $i++) {
				if ($i + 1 == $lastlevel) {
					$table .= "\n</div>"; // derniere fermeture
				} else {
					$table .= "</li>\n</ul>";
				}
			}
		}

		if ($lastlevel >= strlen($level)) {
			// on doit fermer l'item précédent
			if ($cnt[0] > 1 || strlen($level) > 1) {
				$table .= "</li>\n";
			}
		}

		// on se rappelle du raccourcis
		if (isset($ref)) {
			$cite[$ref] = $numeros;
		}
		$table .= "<li><a href=\"$url_article#$ancre\" title=\""
			. _T('itdm:aller_directement_a_', ['titre' => attribut_html($titre)])
			. "\">$titre_lien</a>"
			;

		// on mémorise le niveau de ce titre
		$lastlevel = strlen($level);

		// on génère la balise avec le bon style pour le niveau
		// et on ajoute $level_base à $lastlevel pour avoir des <hx> qui commencent à <h{$level_base}>
		$mdebut_intertitre = str_replace('%num%', $lastlevel + $level_base, $css_debut_intertitre);
		$mfin_intertitre = str_replace('%num%', $lastlevel + $level_base, $css_fin_intertitre);

		// Remplacer la première occurence.
		// Permet d'avoir plusieurs inter-titres au contenu identique.
		$search = str_replace("'", '\'', $matches[0][$j]);

		if ($ancre && $search && (($pos = strpos($texte, $search)) !== false)) {
			#spip_log ('search: '.$search.' pos: '.$pos,'itdm');

			$len_search = strlen($search);
			$s = substr($texte, 0, $pos);
			$s .= $mdebut_intertitre
				// l'ancre nommée (avec mots du titre)
				. '<a id="' . $ancre . '" name="' . $ancre . '"></a>'
				// conserver les ancres compatibles avec les modèles présents
				// dans le plugin (renvoi, extrait)
				. '<a id="a' . $numeros . '" name="a' . $numeros . '"></a>'
				. $titre
				. $mfin_intertitre;
			$s .= substr($texte, $pos + $len_search);
			$texte = $s;
		}
	}

	// on finit la table
	for ($i = 0; $i < $lastlevel; $i++) {
		$table .= "</li>\n</ul>";
	}

	// on remplace les raccourcis par les numéros des sections.
	foreach ($cite as $ref => $num) {
		$texte = str_replace("<$ref>", "<a href=\"$url_article#$num\">$num</a>", $texte);
	}

	// ajout d'un div plus propre !
	$table .= "\n</div>";

	// on place la table des matières dans le texte
	// si y'a rien, ben on envoie rien !
	if ($cnt[0] == 0) {
		$table = '';
	}

	// Comme la TDM est désormais affichée de manière externe aux articles, si on met #TABLEMATIERES
	// dans son article, celà crée un lien vers la TDM externe, d'où un remplacement de:
	// $texte = str_replace('#TABLEMATIERES',$table,$texte); par:
	$texte = str_replace('#TABLEMATIERES', '<a href="#table_des_matieres" id="table_des_matieres_renvoi" title="' . _T('itdm:title_lien_table_matieres') . '">' . _T('itdm:titre_table_matieres') . '</a>', $texte);

	// si tableseule on ne renvoit que la table, sinon, on renvoie tout
	if ($tableseule) {
		return $table;
	}
	return $texte;
}

/**
 * Calcul de l'ancre.
 * Réalisé à la première passe
 * (ce script est appelé par les balises #TEXTE du squelette)
 * Aux passages suivants, donne l'ancre calculée à la première passe.
 *
 * @author Christian Paulus
 * @param int $pass num du passe (de l'appel du script)
 * @param int $pos position du titre dans le document
 * @return string l'ancre composée
 */
function intertitrestdm_composer_ancre(?string $titre, string $pass, int $pos) {
	$titre = $titre ?? '';
	static $ancres_locales = [];
	$ancre = '';

	// Si l'ancre a déjà été calculée dans
	// un précedant passage, renvoyer le résultat
	if (isset($ancres_locales[$pos])) {
		$ancre = $ancres_locales[$pos];
		return ($ancre);
	}

	// Si une ancre est déjà présente, la conserver.
	// Nota: il y a deux façons de forcer une ancre :
	// 1/ ajout de <a id="ancre" name="ancre"></a>
	// 2/ ajout de <monancre> , dans ce cas interprété par le compilo SPIP
	// Dans les deux cas, il faut placer cet ajout dans l'inter-titre.
	// Par exemple : {{{Mon inter-titre <monancre>}}}
	if (($ii = strpos($titre, '<a id=')) !== false) {
		$ancre = substr($titre, $ii + 6);
		$cc = substr($ancre, 0, 1);
		$ancre = substr($ancre, 1);
		$ii = strpos($ancre, $cc);
		$ancre = substr($ancre, 0, $ii);
	}

	if (!strlen($ancre)) {
		// Traduire le titre en ascii 7 bits
		$titre = preg_replace('{([^[:alnum:]]+)}', ' ', translitteration(trim(strip_tags($titre))));

		// Calculer l'ancre à partir du titre
		foreach (explode(' ', $titre) as $mot) {
			// Ne pas traiter les mots trop courts
			if (strlen($mot) <= 2) {
				continue;
			}
			// Sinon, rajouter à la suite de l'ancre
			$ancre .= $mot . '-';
			// Dans la limite acceptable
			if (strlen($ancre) >= _ANCHOR_LEN_MAX) {
				break;
			}
		}
		$ancre = rtrim($ancre, '-');

		// Si inter-titre vide (c'est possible ?) baptiser 'ancre'
		if (!strlen($ancre)) {
			$ancre = 'ancre';
		}

		// Les ancres devraient etre case-insensitives
		// Autant tout passer en bas de casse
		$ancre = strtolower($ancre);
	}

	//$ancre_calcule = $ancre.($pos ? '-'.$pos : '');
	$ancre_calcule = $ancre . '-' . $pos;
	$ancres_locales[$pos] = $ancre_calcule;

	return $ancre_calcule;
}

/**
 * Supprimer les tags d'une chaine en conservant certains tags typos
 *
 * @param string $str
 * @return string
 */
function intertitrestdm_preserver_tags_typo(?string $str): string {
	$str = $str ?? '';
	// passe 1: on echappe les tags autorisés: i, em, strong, sub, sup
	$pattern = '(i|em|strong|sub|sup|/i|/em|/strong|/sub|/sup)';
	$str = preg_replace('#<' . $pattern . '#', 'ßß$1', $str);
	$str = preg_replace('#' . $pattern . '>#', '$1γγ', $str);

	// on vire les autres tags
	$str = trim(strip_tags($str));

	// on retablit les tags echappés
	$str = preg_replace('#ßß' . $pattern . '#', '<$1', $str);
	$str = preg_replace('#' . $pattern . 'γγ#', '$1>', $str);

	return $str;
}
